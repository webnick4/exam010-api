const express = require("express");
const router = express.Router();

const createRouter = db => {

  router.get('/', (req, res) => {
    const news_id = req.param('news_id');

    if (news_id) {
      db.query('SELECT * FROM `comments` WHERE `news_id`= "'+ news_id + '"', function (error, results) {
        if (error) throw error;
        res.send(results);
      })
    } else {
      db.query('SELECT *  FROM `comments`', function (error, results) {
        if (error) throw error;
        res.send(results);
      })
    }
  });


  router.post('/', (req, res) => {
    const data = req.body;

    db.query('SELECT * FROM `news` WHERE `id`= "' + data.news_id + '"', (error, results) => {
      if (error) throw error;

      if (results.length !== 0) {
        if (data.comment) {
          if (data.author === '') {
            data.author = 'Anonymous';
          }

          db.query('INSERT INTO `comments` (`news_id`, `author`, `comment`) VALUES (?, ?, ?)',
            [data.news_id, data.author, data.comment],
            (error, results) => {
              if (error) throw error;
              data.id = results.insertId;
              res.send(data);
            }
          )
        } else {
          res.status(400).send('Field "Comment" can not be blank');
        }
      } else {
        res.status(400).send('News not found');
      }
    });
  });

  router.delete('/:id', (req, res) => {
    const id = req.params.id;

    db.query('DELETE FROM `comments` WHERE `id`= "'+ id + '"', (error, results) => {
      if (error) throw error;
      res.send(results);
    });
  });


  return router;
};

module.exports = createRouter;