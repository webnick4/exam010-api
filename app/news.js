const express = require("express");
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');
const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const createRouter = db => {

  router.get('/', (req, res) => {
    db.query('SELECT *  FROM `news`', function (error, results) {
      if (error) throw error;
      res.send(results);
    })
  });

  router.post('/', upload.single('image'), (req, res) => {
    const data = req.body;
    if (req.file) {
      data.image = req.file.filename;
    } else {
      data.image = null;
    }

    let query = '';
    const params = [data.title, data.post];

    if (data.image) {
      query = 'INSERT INTO `news` (`title`, `post`, `image`) VALUES (?, ?, ?)';
      params.push(data.image);
    } else {
      query = 'INSERT INTO `news` (`title`, `post`) VALUES (?, ?)';
    }

    if (data.title && data.post) {
      db.query(query, params, (error, results) => {
          if (error) throw error;
          data.id = results.insertId;
          res.send(data);
        }
      )
    } else {
      res.status(400).send('Fields "Title", "Post" can not be blank');
    }
  });

  router.get('/:id', (req, res) => {
    const id = req.params.id;

    db.query('SELECT * FROM `news` WHERE `id`= "'+ id + '"', (error, results) => {
      if (error) throw error;
      res.send(results);
    })
  });

  router.delete('/:id', (req, res) => {
    const id = req.params.id;

    db.query('DELETE FROM `news` WHERE `id`= "'+ id + '"', (error, results) => {
      if (error) throw error;
      res.send(results);
    });
  });

  return router;
};

module.exports = createRouter;